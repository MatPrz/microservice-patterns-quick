package com.mateuszprzybyla.presentation.microservice.patterns.discovery;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.HttpEndpoint;
import java.time.LocalDateTime;

public class RestServerVerticle extends AbstractVerticle {
    private static final int PORT = 8082;
    private Record registrationRecord;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        HttpServer httpServer = vertx.createHttpServer();
        httpServer.requestHandler(request -> {
            if (!request.params().contains("time")) {
                request.response().setStatusCode(400);
                request.response().end("time parameter required");
                return;
            }
            int timeMs = Integer.valueOf(request.getParam("time"));
            System.out.println("Request to a path: " + request.absoluteURI());
            request.bodyHandler(buffer -> {
                System.out.println(LocalDateTime.now() + " - Request body " + buffer);
                vertx.setTimer(timeMs, timeId -> {
                    HttpServerResponse response = request.response();
                    response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
                    response.end(Json.encode(new Resource("some very important data")));
                });
            });
        });
        httpServer.listen(PORT, result -> {
            if (result.succeeded()) {
                registerService();
                startFuture.complete();
            } else {
                startFuture.fail("Failed to bind");
            }
        });
    }

    private void registerService() {
        ServiceDiscovery serviceDiscovery = ServiceDiscovery.create(vertx);
        Record restRecord = HttpEndpoint.createRecord("rest-endpoint", "localhost", PORT, "/api/data");
        serviceDiscovery.publish(restRecord, asyncResult -> {
            if (asyncResult.succeeded()) {
                System.out.println("rest-endpoint has been registered, status: " + asyncResult.result().getStatus());
                this.registrationRecord = asyncResult.result();
            } else {
                System.out.println("Failed to register rest-endpoint");
                asyncResult.cause().printStackTrace();
            }
        });
    }


    @Override
    public void stop(Future<Void> stopFuture) throws Exception {
        if (registrationRecord != null) {
            ServiceDiscovery serviceDiscovery = ServiceDiscovery.create(vertx);
            serviceDiscovery.unpublish(registrationRecord.getRegistration(), asyncResult -> {
                if (asyncResult.succeeded()) {
                    System.out.println("rest-endpoint has been unregistered");
                    stopFuture.complete();
                } else {
                    System.out.println("Failed to unregister rest-endpoint");
                    stopFuture.fail(asyncResult.cause());
                }
            });
        } else {
            stopFuture.complete();
        }
    }
}
