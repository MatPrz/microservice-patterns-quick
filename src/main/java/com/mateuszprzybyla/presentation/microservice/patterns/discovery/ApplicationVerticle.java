package com.mateuszprzybyla.presentation.microservice.patterns.discovery;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceReference;
import java.util.concurrent.TimeUnit;

public class ApplicationVerticle extends AbstractVerticle {
    private static final int PORT = 8080;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        startHttpServer(startFuture);
    }

    private void startHttpServer(Future<Void> startFuture) {
        Router router = Router.router(vertx);
        router.get("/").handler(this::renderContent);
        router.get("/services").handler(this::renderServices);

        HttpServer httpServer = vertx.createHttpServer();
        httpServer.requestHandler(router::accept);
        httpServer.listen(PORT, result -> {
            if (result.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail("Failed to bind");
            }
        });
    }

    private void renderContent(RoutingContext context) {
        if (!context.request().params().contains("time")) {
            context.response().end("Time parameter is required");
            return;
        }
        int timeMs = Integer.valueOf(context.request().getParam("time"));
        getRestData(timeMs).setHandler(asyncResult -> {
            if (asyncResult.succeeded()) {
                context.response().end("<h1>Main page</h1><h3>Data: " + asyncResult.result().getData() + "</h3>");
            } else {
                context.response().end("<h1>Main page</h1><h3 style=\"color:red\">"
                    + "Data could not be retrieved :(</h3>" + asyncResult.cause().getMessage());
            }
        });
    }

    private Future<Resource> getRestData(int timeMs) {
        Future<Resource> restDataFuture = Future.future();
        ServiceDiscovery serviceDiscovery = ServiceDiscovery.create(vertx);
        serviceDiscovery.getRecord(new JsonObject().put("name", "rest-endpoint"), asyncResult -> {
            if (asyncResult.succeeded()) {
                Record record = asyncResult.result();
                System.out.println("Service record found: " + record.toJson());
                ServiceReference reference = serviceDiscovery.getReference(record);
                HttpClient client = reference.get();
                HttpClientRequest request = client.get("/api/data?time=" + timeMs);
                request.exceptionHandler(restDataFuture::fail);
                request.setTimeout(TimeUnit.SECONDS.toMillis(3));
                request.handler(response -> {
                    response.bodyHandler(buffer -> {
                        System.out.println("Retrieved rest data: " + buffer);
                        restDataFuture.complete(Json.decodeValue(buffer.toString(), Resource.class));
                    });
                });
                request.end();
            } else {
                System.out.println("Could not discover rest-endpoint");
                restDataFuture.fail(asyncResult.cause());
            }

        });
        return restDataFuture;
    }

    private void renderServices(RoutingContext context) {
        ServiceDiscovery serviceDiscovery = ServiceDiscovery.create(vertx);
        serviceDiscovery.getRecords(r -> true, true, asyncResult -> {
            if (asyncResult.succeeded()) {
                StringBuilder responseBuilder = new StringBuilder("<html>Services: ").append("<br/>");
                asyncResult.result().forEach(result -> responseBuilder.append(result.toJson()).append("<br/>"));
                responseBuilder.append("</html>");
                context.response().end(responseBuilder.toString());
            } else {
                context.response().end("Failed to retrieved records");
            }
        });
    }
}