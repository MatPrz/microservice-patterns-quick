package com.mateuszprzybyla.presentation.microservice.patterns.circuit;

import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

public class ApplicationVerticle extends AbstractVerticle {
    private static final int PORT = 8080;
    private static final int REST_PORT = 8082;
    private HttpClient restClient;
    private CircuitBreaker circuitBreaker;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        this.circuitBreaker = prepareCircuitBreaker();
        this.restClient = prepareRestClient();
        startHttpServer(startFuture);
    }

    private CircuitBreaker prepareCircuitBreaker() {
        CircuitBreakerOptions options = new CircuitBreakerOptions()
            .setTimeout(TimeUnit.SECONDS.toMillis(3))
            .setFallbackOnFailure(true)
            .setResetTimeout(TimeUnit.SECONDS.toMillis(10))
            .setMaxFailures(1);
        return CircuitBreaker.create("rest-circuit-breaker", vertx, options)
            .openHandler(v -> System.out.println(LocalDateTime.now() + " - Circuit is now OPEN"))
            .closeHandler(v -> System.out.println(LocalDateTime.now() + " - Circuit is now CLOSED"))
            .halfOpenHandler(v -> System.out.println(LocalDateTime.now() + " - Circuit is now HALF OPEN"))
            .fallback(t -> {
                System.out.println(LocalDateTime.now() + " - Fallback callback, error: " + t.getMessage());
                return new Resource("default offline data");
            });
    }

    private HttpClient prepareRestClient() {
        HttpClientOptions clientOptions = new HttpClientOptions()
            .setDefaultHost("localhost")
            .setDefaultPort(REST_PORT);
        return vertx.createHttpClient(clientOptions);
    }

    private void startHttpServer(Future<Void> startFuture) {
        Router router = Router.router(vertx);
        router.get("/").handler(this::renderContent);

        HttpServer httpServer = vertx.createHttpServer();
        httpServer.requestHandler(router::accept);
        httpServer.listen(PORT, result -> {
            if (result.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail("Failed to bind");
            }
        });
    }

    private void renderContent(RoutingContext context) {
        if (!context.request().params().contains("time")) {
            context.response().end("Time parameter is required");
            return;
        }
        int timeMs = Integer.valueOf(context.request().getParam("time"));
        getRestData(timeMs).setHandler(asyncResult -> {
            if (asyncResult.succeeded()) {
                context.response().end("<h1>Main page</h1><h3>Data: " + asyncResult.result().getData() + "</h3>");
            } else {
                context.response().end("<h1>Main page</h1><h3 style=\"color:red\">"
                    + "Data could not be retrieved :(</h3>" + asyncResult.cause().getMessage());
            }
        });
    }

    private Future<Resource> getRestData(int timeMs) {
        return circuitBreaker.execute(restDataFuture -> {
            HttpClientRequest request = restClient.get("/api/data?time=" + timeMs);
            request.exceptionHandler(restDataFuture::fail);
            request.handler(response -> {
                response.bodyHandler(buffer -> {
                    System.out.println("Retrieved rest data: " + buffer);
                    restDataFuture.complete(Json.decodeValue(buffer.toString(), Resource.class));
                });
            });
            request.end();
        });
    }
}