package com.mateuszprzybyla.presentation.microservice.patterns.circuit;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import java.time.LocalDateTime;

public class RestServerVerticle extends AbstractVerticle {
    private static final int PORT = 8082;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        HttpServer httpServer = vertx.createHttpServer();
        httpServer.requestHandler(request -> {
            if (!request.params().contains("time")) {
                request.response().setStatusCode(400);
                request.response().end("time parameter required");
                return;
            }
            int timeMs = Integer.valueOf(request.getParam("time"));
            request.bodyHandler(buffer -> {
                System.out.println(LocalDateTime.now() + " - Request body " + buffer);
                vertx.setTimer(timeMs, timeId -> {
                    HttpServerResponse response = request.response();
                    response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json");
                    response.end(Json.encode(new Resource("some very important data")));
                });
            });
        });
        httpServer.listen(PORT, result -> {
            if (result.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail("Failed to bind");
            }
        });
    }
}
